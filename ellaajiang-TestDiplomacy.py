#!/usr/bin/env python3


from io import StringIO
from unittest import main, TestCase

from Diplomacy import read_input, execute_actions, return_output, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = ["A City1 Move City2"]
        j = read_input(s)
        self.assertEqual(j,[("A","City1","Move","City2")])
        
    def test_read2(self):
        s = ["A City1 Move City2","B City2 Hold","C City3 Support A"]
        j = read_input(s)
        self.assertEqual(j,[("A","City1","Move","City2"),("B","City2","Hold"),("C","City3","Support","A")])
        

    def test_read3(self):
        j = read_input([])
        self.assertEqual(j,[])

    def test_read4(self):
        s = ["C Houston Move Dallas","B Austin Hold","A SF Support B"]
        j = read_input(s)
        self.assertEqual(j,[("C","Houston","Move","Dallas"),("B","Austin","Hold"),("A","SF","Support","B")])

    def test_read5(self):
        s = ["M Dallas Move Austin","S SA Move Miami","R Houston Support M","C Boston Move SA", "L LA Support R", "H Miami Move SA", "P Indiana Move Boston","W SF Support S","U Phoenix Move Houston"]
        j = read_input(s)
        self.assertEqual(j,[('M', 'Dallas', 'Move', 'Austin'), ('S', 'SA', 'Move', 'Miami'), ('R', 'Houston', 'Support', 'M'), ('C', 'Boston', 'Move', 'SA'), ('L', 'LA', 'Support', 'R'), ('H', 'Miami', 'Move', 'SA'), ('P', 'Indiana', 'Move', 'Boston'), ('W', 'SF', 'Support', 'S'), ('U', 'Phoenix', 'Move', 'Houston')])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        b = [('M', 'Dallas', 'Move', 'Austin'), ('S', 'SA', 'Move', 'Miami'), ('R', 'Houston', 'Support', 'M'), ('C', 'Boston', 'Move', 'SA'), ('L', 'LA', 'Support', 'R'), ('H', 'Miami', 'Move', 'SA'), ('P', 'Indiana', 'Move', 'Boston'), ('W', 'SF', 'Support', 'S'), ('U', 'Phoenix', 'Move', 'Houston')]
        answer = {'M': 'Austin', 'S': 'Miami', 'R': 'Houston', 'C': '[dead]', 'L': 'LA', 'H': '[dead]', 'P': 'Boston', 'W': 'SF', 'U': '[dead]'}
        self.assertEqual(execute_actions(b),answer)

    def test_eval_2(self):
        b = [('A', '1', 'Support', 'B'), ('B', '2', 'Support', 'A'), ('C', '3', 'Move', '1'), ('D', '4', 'Move', '2'), ('E', '5', 'Support', 'C'),('F', '6', 'Support', 'D'), ('G', '7', 'Move', '5'), ('H', '8', 'Move', '6')]
        ans = execute_actions(b)
        ans1 = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': '[dead]', 'E': '[dead]', 'F': '[dead]', 'G': '[dead]', 'H': '[dead]'}
        self.assertEqual(ans,ans1)

    def test_eval_3(self):
        b = [('A', '1', 'Hold'), ('B', '2', 'Hold'), ('C', '3', 'Move', '1'), ('D', '4', 'Move', '2'), ('E', '5', 'Support', 'C'), ('F', '6', 'Support', 'D'), ('G', '7', 'Hold'), ('H', '8', 'Move', '6')]
        ans = {'A': '[dead]', 'B': '[dead]', 'C': '1', 'D': '[dead]', 'E': '5', 'F': '[dead]', 'G': '7', 'H': '[dead]'}
        self.assertEqual(ans,execute_actions(b))

    def test_eval_4(self):
        b = [('A', 'Madrid', 'Hold'), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Move', 'Madrid'), ('D', 'Paris', 'Support', 'B'), ('E', 'Dublin', 'Support', 'D'), ('F', 'Dallas', 'Move', 'Paris')]
        ans = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': 'Paris', 'E': 'Dublin', 'F': '[dead]'}
        self.assertEqual(ans,execute_actions(b))
    def test_eval_5(self):
        b = [('A','Madrid','Support','B'), ('B', 'Barcelona', 'Move', 'Madrid')]
        ans = {'A': '[dead]', 'B': '[dead]'}
        self.assertEqual(ans,execute_actions(b))
    def test_eval_6(self):
        b = [('A', 'Madrid', 'Hold'), ('B', 'Barcelona', 'Move', 'Madrid'), ('C', 'London', 'Move', 'Madrid'), ('D', 'Paris', 'Support', 'B'), ('E', 'Dublin', 'support', 'D')]
        ans = {'A': '[dead]', 'B': 'Madrid', 'C': '[dead]', 'D': 'Paris', 'E': 'Dublin'}
        self.assertEqual(ans,execute_actions(b))

    # -----
    # print
    # -----

    def test_print1(self):
        b = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': 'Paris', 'E': 'Dublin', 'F': '[dead]'}
        a = return_output(b)
        c = 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Dublin\nF [dead]'
        self.assertEqual(a,c)

    def test_print2(self):
        b = {"J":"phonehome","E":"inTheCPE"}
        a = return_output(b)
        c = 'E inTheCPE\nJ phonehome'
        self.assertEqual(a,c)

    def test_print3(self):
        b = {"A":"I","B":"Want","C":"To","D":"Go","E":"Home"}
        a = return_output(b)
        c = 'A I\nB Want\nC To\nD Go\nE Home'
        self.assertEqual(a,c)

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Move Home")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Home")

    def test_solve2(self):
        r = StringIO("M Dallas Move Austin\nS SA Move Miami\nR Houston Support M\nC Boston Move SA\nL LA Support R\n\n\n\nH Miami Move SA\nP Indiana Move Boston\nW SF Support S\nU Phoenix Move Houston")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'C [dead]\nH [dead]\nL LA\nM Austin\nP Boston\nR Houston\nS Miami\nU [dead]\nW SF')

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\nF Dallas Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        c = 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Dublin\nF [dead]'
        self.assertEqual(w.getvalue(),c)
    def test_solve4(self):
        r = StringIO("A Madrid Support B\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        c = 'A [dead]\nB [dead]'
        self.assertEqual(w.getvalue(),c)


# ----
# main
# ----


if __name__ == "__main__":
    main()

